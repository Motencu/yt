const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const CardSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  type: {
    type: Number,
    default: 0,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  correctId: {
    type: String
  },
  audio: {
    type: String
  },
  options: [
    {
      text: {
        type: String,
        required: true
      },
      date: {
        type: Date,
        default: Date.now
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Card = mongoose.model('card', CardSchema);
