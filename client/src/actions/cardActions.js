import Axios from 'axios';

import {
  ADD_CARD,
  GET_CARDS,
  CARD_LOADING,
  GET_ERRORS,
  DELETE_CARD,
  GET_CARD
} from './types';

// post card
export const addCard = cardData => dispatch => {
  Axios.post('../api/cards', cardData)
    .then(res => dispatch({ type: ADD_CARD, payload: res.data }))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// get cards
export const getCards = () => dispatch => {
  dispatch(setCardLoaing());
  Axios.get('../api/cards')
    .then(res => dispatch({ type: GET_CARDS, payload: res.data }))
    .catch(err =>
      dispatch({
        type: GET_CARDS,
        payload: null
      })
    );
};

// get card by id
export const getCard = id => dispatch => {
  dispatch(setCardLoaing());
  Axios.get(`../../api/cards/${id}`)
    .then(res => dispatch({ type: GET_CARD, payload: res.data }))
    .catch(err => {
      dispatch({
        type: GET_CARD,
        payload: null
      });
      console.log(err);
    });
};

// delete card
export const deleteCard = id => dispatch => {
  Axios.delete(`../api/cards/${id}`)
    .then(res => dispatch({ type: DELETE_CARD, payload: id }))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// add option
export const addOption = (cardId, optionData) => dispatch => {
  Axios.post(`../../api/cards/options/${cardId}`, optionData)
    .then(res => dispatch({ type: GET_CARD, payload: res.data }))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// delete option
export const deleteOption = (cardId, optionId) => dispatch => {
  Axios.delete(`../../api/cards/options/${cardId}/${optionId}`)
    .then(res => dispatch({ type: GET_CARD, payload: res.data }))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// make option correct
export const correctOption = (cardId, optionId) => dispatch => {
  Axios.put(`../../api/cards/correct/${cardId}/${optionId}`)
    .then(res => dispatch({ type: GET_CARD, payload: res.data }))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// set loading state
export const setCardLoaing = () => {
  return {
    type: CARD_LOADING
  };
};
