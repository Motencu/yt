import { combineReducers } from 'redux';
import errorReducer from './errorReducer';
import authReducer from './authReducer';
import cardReducer from './cardReducer';

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  card: cardReducer
});
