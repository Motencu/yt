import {
  ADD_CARD,
  GET_CARDS,
  GET_CARD,
  CARD_LOADING,
  DELETE_CARD
} from '../actions/types';

const initialState = {
  cards: [],
  card: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CARD_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_CARDS:
      return {
        ...state,
        cards: action.payload,
        loading: false
      };
    case GET_CARD:
      return {
        ...state,
        card: action.payload,
        loading: false
      };
    case ADD_CARD:
      return {
        ...state,
        cards: [action.payload, ...state.cards]
      };
    case DELETE_CARD:
      return {
        ...state,
        cards: state.cards.filter(card => card._id !== action.payload)
      };
    default:
      return state;
  }
}
