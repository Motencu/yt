import React from 'react';
import { Link } from 'react-router-dom';

const VideoDisplay = props => {
  return (
    <div className="container-fluid video-display">
      <div className="row">
        <div className="col-1  text-right p-0">
          <div className="side-nav">
            <Link className="btn-side" to="/">
              <i className="fas fa-home side-button" />
            </Link>
            <br />
            <a className="btn-side" href="#video-list">
              <i className="fab fa-youtube side-button" />
            </a>
          </div>
        </div>
        <div className="col-10 text-center p-0">
          <div className="red">
            <iframe
              title="video"
              width="796"
              height="448"
              src={
                'https://www.youtube.com/embed/' + props.selectedVideo.videoID
              }
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default VideoDisplay;
