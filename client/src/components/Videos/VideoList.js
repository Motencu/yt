import React from 'react';
import VideoListThumbnail from './VideoListThumbnail';

const VideoList = props => {
  let videos = props.videos.map(video => {
    return (
      <VideoListThumbnail
        key={video._id}
        video={video}
        onVideoSelect={props.onVideoSelect}
      />
    );
  });

  return (
    <div id="video-list" className="col-md-3">
      <div className="row">{videos}</div>
    </div>
  );
};

export default VideoList;
