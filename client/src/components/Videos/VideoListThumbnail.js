import React from 'react';

const VideoListThumbnail = props => {
  return (
    <div class="row" onClick={() => props.onVideoSelect(props.video)}>
      <div class="col-5 p-1 pl-4">
        <img
          src={'https://i.ytimg.com/vi/' + props.video.audio + '/mqdefault.jpg'}
          alt=""
          className="img-fluid"
        />
      </div>
      <div class="col-7 p-1 side-title">{props.video.text}</div>
    </div>
  );
};

export default VideoListThumbnail;
