import React, { Component } from 'react';
import VideoList from './VideoList';
import VideoDisplay from './VideoDisplay';

class VideoMain extends Component {
  constructor() {
    super();
    this.state = {
      selectedVideo: {
        videoID: 'PE7sW2KoiGI'
      },
      videos: [
        {
          videoID: 'dKv6bEkgILE'
        },
        {
          videoID: 'Y3cPq5vmXng'
        },
        {
          videoID: '3aYemty3pIc'
        },
        {
          videoID: 'Mn5h0Jdig1U'
        },
        {
          videoID: 'jXBLU7g4elQ'
        },
        {
          videoID: 'mjcjP-vRriA'
        }
      ]
    };
  }

  render() {
    return (
      <div>
        <VideoDisplay selectedVideo={this.state.selectedVideo} />
        <VideoList
          videos={this.state.videos}
          onVideoSelect={selectedVideo => {
            this.setState({ selectedVideo });
            console.log('it twerks');
            window.scrollTo(0, 0);
          }}
        />
      </div>
    );
  }
}

export default VideoMain;
