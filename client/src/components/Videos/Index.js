import React, { Component } from 'react';
import classnames from 'classnames';
import VideoList from './VideoList';
import Axios from 'axios';

class Index extends Component {
  constructor() {
    super();
    this.state = {
      text: '',
      filter: '',
      video: {
        audio: 'AJKJV9j_uy0',
        text: 'How to Draw Naruto | Sketch Tutorial'
      },
      videos: [],
      fullScreen: false
    };
  }

  componentDidMount() {
    Axios.get('../api/cards')
      .then(res => {
        this.setState({ videos: res.data });
        console.log(this.state.vidoes);
      })
      .catch(err => console.log(err));
  }
  render() {
    const prefilter = this.state.videos.filter(video =>
      video.text.toLowerCase().includes(this.state.filter.toLowerCase())
    );

    const filter = prefilter.splice(0, 15);
    return (
      <div>
        <title>{this.state.video.text}</title>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
            {' '}
            <a class="navbar-brand" href="#">
              <img src="https://i.imgur.com/09YfyLv.png" alt="" />
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon" />
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <button
                    class="btn"
                    onClick={() => {
                      const grabBoo = !this.state.fullScreen;
                      this.setState({ fullScreen: grabBoo });
                      console.log(this.state.fullScreen);
                    }}
                  >
                    <i class="fas fa-circle" />
                  </button>
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <input
                  class="form-control mr-sm-2"
                  value={this.state.text}
                  onChange={e => this.setState({ text: e.target.value })}
                  type="search"
                  placeholder="Search"
                  aria-label="Search "
                />
                <button
                  class="btn btn-outline-success my-2 my-sm-0"
                  type="submit"
                  onClick={e => {
                    e.preventDefault();
                    this.setState({ filter: this.state.text });
                  }}
                >
                  <i class="fas fa-search" />
                </button>
              </form>
            </div>
          </div>
        </nav>

        <main
          className={classnames('container-fluid', {
            black: this.state.fullScreen
          })}
        >
          <div class="row">
            <div
              className={classnames('col-md-9', {
                'col-md-12': this.state.fullScreen
              })}
            >
              <div class="video-container">
                <iframe
                  title="yt"
                  class="video"
                  width="560"
                  height="315"
                  src={
                    'https://www.youtube.com/embed/' + this.state.video.audio
                  }
                  frameborder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen
                />
              </div>
              <div class="row yt-title">
                <div class="col-md-6">
                  <h4>{this.state.video.text}</h4>
                </div>
                <div class="col-md-6 text-right">
                  <button class="btn btn-red">
                    <i class="fas fa-heart" /> Add To Favorites
                  </button>
                </div>
              </div>
            </div>
            <VideoList
              term={this.state.text}
              videos={filter}
              onVideoSelect={video => {
                this.setState({ video });
                window.scrollTo(0, 0);
                document.title = this.state.video.text;
              }}
            />
          </div>
        </main>
      </div>
    );
  }
}

export default Index;
