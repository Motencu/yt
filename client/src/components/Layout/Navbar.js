import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';

class Navbar extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { isAuthenicated, user } = this.props.auth;

    const authLinks = (
      <button onClick={this.onLogoutClick}>Log Out ({user.name})</button>
    );

    const guestLinks = (
      <div>
        <Link to="/login">Login</Link> - <Link to="/register">Register</Link>
      </div>
    );

    return (
      <div className="container">
        <h1>Welcome</h1>
        <div>{isAuthenicated ? authLinks : guestLinks}</div>
        <div>{user.isAdmin ? <Link to="/admin">Admin Panel</Link> : null}</div>
      </div>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Navbar);
