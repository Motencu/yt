import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from './Layout/Navbar';

const Menu = () => {
  return (
    <div className="container-fluid text-center">
      <Navbar />
      <Link className="btn btn-lg btn-primary" to="/play">
        Videos
      </Link>
      <br />
      <br />
      <Link className="btn btn-lg btn-primary" to="/quiz">
        Quiz
      </Link>

      <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/lha9hsx5roY"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      />
    </div>
  );
};

export default Menu;
