import React from 'react';
import AdminNavbar from '../Layout/AdminNavbar';

const Dashboard = () => {
  return (
    <div>
      <AdminNavbar />
    </div>
  );
};

export default Dashboard;
