import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCard } from '../../../actions/cardActions';

import AdminNavbar from '../../Layout/AdminNavbar';
import OptionForm from '../../Admin/Card/OptionForm';
import OptionList from '../../Admin/Card/OptionList';

class Card extends Component {
  componentDidMount() {
    this.props.getCard(this.props.match.params.id);
  }
  render() {
    const { card, loading } = this.props.card;
    let cardContent;

    if (card === null || loading || Object.keys(card).length === 0) {
      cardContent = 'loading..';
    } else {
      cardContent = (
        <div>
          <p>Question: {card.text}</p>
          <OptionList cardId={card._id} options={card.options} />
          <OptionForm cardId={card._id} />
        </div>
      );
    }

    return (
      <div>
        <AdminNavbar />
        <div className="container">
          <h1>Single Card</h1>
          {cardContent}
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  getCard: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  card: state.card
});

export default connect(
  mapStateToProps,
  { getCard }
)(Card);
