import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteOption } from '../../../actions/cardActions';
import { correctOption } from '../../../actions/cardActions';

class OptionItem extends Component {
  onCorrectClick = (cardId, optionId) => {
    this.props.correctOption(cardId, optionId);
  };

  onDeleteClick = (cardId, optionId) => {
    this.props.deleteOption(cardId, optionId);
  };
  render() {
    const { option, cardId, correct } = this.props;

    let correctBtn;

    if (correct === option._id) {
      correctBtn = (
        <span className="text-success font-weight-bold">Correct</span>
      );
    } else {
      correctBtn = (
        <button
          onClick={() => this.onCorrectClick(cardId, option._id)}
          className="btn btn-sm btn-secondary"
        >
          Make Correct
        </button>
      );
    }

    return (
      <div className="list-item">
        <div className="row">
          <div className="col-8">{option.text}</div>
          <div className="col-4 text-right">
            {correctBtn}{' '}
            <button
              onClick={() => this.onDeleteClick(cardId, option._id)}
              type="button"
              className="btn btn-sm btn-danger"
            >
              x
            </button>
          </div>
        </div>
      </div>
    );
  }
}

OptionItem.propTypes = {
  deleteOption: PropTypes.func.isRequired,
  option: PropTypes.object.isRequired,
  cardId: PropTypes.string.isRequired,
  correct: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  correct: state.card.card.correctId
});

export default connect(
  mapStateToProps,
  { deleteOption, correctOption }
)(OptionItem);
