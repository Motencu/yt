import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '../../common/TextField';

import { addOption } from '../../../actions/cardActions';

class OptionForm extends Component {
  constructor() {
    super();
    this.state = {
      text: '',
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const { cardId } = this.props;

    const newOption = {
      text: this.state.text
    };
    this.props.addOption(cardId, newOption);
    this.setState({ text: '' });
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="card">
        <div className="card-header">Add an option</div>

        <div className="card-body">
          <form onSubmit={this.onSubmit}>
            <TextField
              placeholder="Add option"
              name="text"
              value={this.state.text}
              onChange={this.onChange}
              error={errors.text}
            />
            <button className="btn btn-dark" type="submit">
              Add Card
            </button>
          </form>
        </div>
      </div>
    );
  }
}

OptionForm.propTypes = {
  addCard: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  cardId: PropTypes.string.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addOption }
)(OptionForm);
