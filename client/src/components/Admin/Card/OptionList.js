import React, { Component } from 'react';
import PropTypes from 'prop-types';
import OptionItem from './OptionItem';

class OptionList extends Component {
  render() {
    const { options, cardId } = this.props;
    return options.map(option => (
      <OptionItem key={option._id} option={option} cardId={cardId} />
    ));
  }
}

OptionList.propTypes = {
  options: PropTypes.array.isRequired,
  cardId: PropTypes.string.isRequired
};

export default OptionList;
