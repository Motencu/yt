import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from '../../common/TextField';

import { addCard } from '../../../actions/cardActions';

class CardForm extends Component {
  constructor() {
    super();
    this.state = {
      text: '',
      audio: '',
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const newCard = {
      text: this.state.text,
      audio: this.state.audio
    };

    this.props.addCard(newCard);
    this.setState({ text: '' });
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="card">
        <div className="card-header">Add new youtube video</div>

        <div className="card-body">
          <form onSubmit={this.onSubmit}>
            <TextField
              placeholder="Video Id"
              name="audio"
              value={this.state.audio}
              onChange={this.onChange}
              error={errors.audio}
            />

            <TextField
              placeholder="Video Title"
              name="text"
              value={this.state.text}
              onChange={this.onChange}
              error={errors.text}
            />
            <button className="btn btn-dark" type="submit">
              Add Video
            </button>
          </form>
        </div>
      </div>
    );
  }
}

CardForm.propTypes = {
  addCard: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addCard }
)(CardForm);
