import React, { Component } from 'react';
import AdminNavbar from '../../Layout/AdminNavbar';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCards } from '../../../actions/cardActions';

import CardList from './CardList';
import CardForm from './CardForm';

class Cards extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.getCards();
  }

  render() {
    const { cards, loading } = this.props.card;
    let cardContent;

    if (cards === null || loading) {
      cardContent = 'loading...';
    } else {
      cardContent = <CardList cards={cards} />;
    }
    return (
      <div>
        <AdminNavbar />
        <div className="container">
          <CardForm />

          <div className="pt-4">{cardContent}</div>
        </div>
      </div>
    );
  }
}

CardForm.propTypes = {
  getCards: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  card: state.card
});

export default connect(
  mapStateToProps,
  { getCards }
)(Cards);
