import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CardItem from './CardItem';

class CardList extends Component {
  render() {
    const { cards } = this.props;
    return cards.map(card => <CardItem key={card._id} card={card} />);
  }
}

CardList.propTypes = {
  cards: PropTypes.array.isRequired
};
export default CardList;
