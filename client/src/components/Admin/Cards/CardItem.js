import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteCard } from '../../../actions/cardActions';

class CardItem extends Component {
  onDeleteClick = id => {
    this.props.deleteCard(id);
  };

  render() {
    const { card } = this.props;
    return (
      <div className="list-item">
        <div className="row">
          <div className="col-8">
            {card.text} | {card.audio}
          </div>
          <div className="col-4 text-right">
            <button
              onClick={() => this.onDeleteClick(card._id)}
              type="button"
              className="btn btn-sm btn-danger"
            >
              x
            </button>
          </div>
        </div>
      </div>
    );
  }
}

CardItem.propTypes = {
  deleteCard: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { deleteCard }
)(CardItem);
