import React, { Component } from 'react';
import Icon from './GameIcon';
import GameFooter from './GameFooter';
import Sounds from './Sounds';

class Play extends Component {
  constructor() {
    super();
    this.state = {
      image: 'https://i.imgur.com/d0BYyz9.png',
      numbers: [
        'zero',
        'one',
        'two',
        'three',
        'four',
        'five',
        'six',
        'seven',
        'eight',
        'nine',
        'ten'
      ],
      icons: [
        {
          id: 1,
          count: 0,
          clicked: false
        },
        {
          id: 2,
          count: 0,
          clicked: false
        },
        {
          id: 3,
          count: 0,
          clicked: false
        },
        {
          id: 4,
          count: 0,
          clicked: false
        },
        {
          id: 5,
          count: 0,
          clicked: false
        }
      ],
      taskComplete: false
    };
  }

  onClickIcon = id => {
    if (!this.state.taskComplete) {
      let newIcons = this.state.icons;

      const indexOfIcon = this.state.icons.map(icon => icon.id).indexOf(id);

      // finds how many icons are clicked/active and adds 1
      const iconCounter = this.state.icons.filter(v => v.clicked).length + 1;

      // find index of biggest count
      const indexOfLatest = this.state.icons
        .map(icon => icon.count)
        .indexOf(iconCounter - 1);

      // toggles icon
      newIcons[indexOfIcon].clicked = !newIcons[indexOfIcon].clicked;

      // inserts count number
      newIcons[indexOfIcon].count = iconCounter;

      // checks if icon is being toggled off
      if (!newIcons[indexOfIcon].clicked && id !== newIcons[indexOfLatest].id) {
        // reset counter
        let count = 1;
        newIcons.forEach(icon => {
          if (icon.clicked) {
            icon.count = count;
            count++;
          }
        });
      }

      this.setState({ icons: newIcons });

      const updatedCounter = this.state.icons.filter(v => v.clicked).length;

      if (updatedCounter !== 0) {
        const currentNumber = this.state.numbers[updatedCounter];
        Sounds[currentNumber].play();
      }

      if (this.state.icons.length === updatedCounter) {
        this.setState({ taskComplete: true });
      } else {
        this.setState({ taskComplete: false });
      }
    }
  };
  render() {
    let iconsDisplay = this.state.icons.map(icon => (
      <Icon
        key={icon.id}
        icon={icon}
        image={this.state.image}
        onClick={this.onClickIcon}
      />
    ));
    return (
      <div>
        <div id="game-container">
          <div className="container-fluid game">
            <div className="row justify-content-center">{iconsDisplay}</div>
          </div>
        </div>
        <GameFooter
          taskComplete={this.state.taskComplete}
          message="Click all the dogs!"
          btnMessage="Continue"
        />
      </div>
    );
  }
}

export default Play;
