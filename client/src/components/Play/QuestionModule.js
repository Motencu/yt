import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import GameOption from './GameOption';
import GameFooter from './GameFooter';
import Sounds from './Sounds';

class QuestionModule extends Component {
  constructor() {
    super();
    this.state = {
      question: {
        text: 'a',
        ref: 'a'
      },
      options: [
        {
          id: 1,
          text: 'あ',
          clicked: false,
          correct: true,
          ref: 'a'
        },
        {
          id: 2,
          text: 'い',
          clicked: false,
          correct: false,
          ref: 'i'
        },
        {
          id: 3,
          text: 'う',
          clicked: false,
          correct: false,
          ref: 'u'
        },
        {
          id: 4,
          text: 'え',
          clicked: false,
          correct: false,
          ref: 'me'
        }
      ],
      taskComplete: false
    };
  }

  onClickSound = () => {
    // this.state.sounds[this.state.question.ref].play();
    Sounds['animals'].play();
  };

  onClickOption = id => {
    const indexOfOption = this.state.options
      .map(option => option.id)
      .indexOf(id);
    let copyOfOptions = this.state.options;
    copyOfOptions[indexOfOption].clicked = true;
    this.setState({ options: copyOfOptions });

    Sounds[copyOfOptions[indexOfOption].ref].play();

    // check if answer is correct
    if (this.state.options[indexOfOption].correct) {
      this.setState({ taskComplete: true });
    }
  };
  render() {
    let options = this.state.options.map(option => (
      <GameOption
        key={option.id}
        option={option}
        onClick={this.onClickOption}
        taskComplete={this.state.taskComplete}
      />
    ));
    return (
      <div>
        <div className="container-fluid box">
          <div className="row">
            <div className="col-1 text-center p-0">
              <Link className="btn-side" to="/">
                <i className="fas fa-home side-button" />
              </Link>
            </div>
            <div className="col-10 text-center p-0">
              <div id="quiz">
                <h1 onClick={this.onClickSound}>{this.state.question.text}</h1>
                <div className="options">
                  <div className="row">{options}</div>
                </div>
              </div>
            </div>
            <div onClick={this.onClickSound} className="col-1 text-center p-0">
              <i className="fas fa-volume-up side-button" />
            </div>
          </div>
        </div>
        <GameFooter
          taskComplete={this.state.taskComplete}
          message="Click the correct answer."
          btnMessage="Continue"
        />
      </div>
    );
  }
}

export default QuestionModule;
