import React from 'react';
import classnames from 'classnames';

const QuizOption = props => {
  return (
    <div className="col-6">
      <button
        className={classnames(
          'btn btn-answer',
          {
            'btn-correct': props.option.correct && props.option.clicked
          },
          { 'btn-incorrect': !props.option.correct && props.option.clicked }
        )}
        onClick={() => props.onClick(props.option.id)}
        disabled={props.option.clicked || props.taskComplete}
      >
        {props.option.text}
      </button>
    </div>
  );
};

export default QuizOption;
