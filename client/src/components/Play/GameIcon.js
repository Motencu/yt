import React from 'react';
import classnames from 'classnames';

const Icon = props => {
  return (
    <div className="col-2">
      <div className="text-container">
        <h1
          className={classnames('game-number', {
            'text-hidden': !props.icon.clicked
          })}
        >
          {props.icon.count}
        </h1>
      </div>

      <button
        onClick={() => props.onClick(props.icon.id)}
        className={classnames('btn btn-game', {
          'btn-game-active': props.icon.clicked
        })}
      >
        <img className="game-icon" src={props.image} alt="" />
      </button>
    </div>
  );
};

export default Icon;
