import React, { Component } from 'react';
import GameFooter from './GameFooter';

class VideoModule extends Component {
  constructor() {
    super();
    this.state = {
      timer: 113,
      video: 'UgvUPbPSY2g',
      taskComplete: false
    };
  }
  componentDidMount() {
    setInterval(() => {
      if (this.state.timer < 0 && !this.state.taskComplete) {
        this.setState({ taskComplete: true });
      }
      const newTime = this.state.timer - 1;
      this.setState({ timer: newTime });
    }, 1000);
  }
  render() {
    return (
      <div>
        <div className="text-center">
          <iframe
            title="video"
            width="796"
            height="448"
            src={'https://www.youtube.com/embed/' + this.state.video}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
        <GameFooter
          taskComplete={this.state.taskComplete}
          message="Click all the dogs!"
          btnMessage="Continue"
        />
      </div>
    );
  }
}

export default VideoModule;
