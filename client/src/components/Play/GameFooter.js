import React from 'react';

const GameFooter = props => {
  return (
    <div className="game-footer">
      <div className="row">
        <div className="col-8">{props.message}</div>
        <div className="col-4 text-right">
          <button
            className="btn btn-primary btn-game-footer"
            disabled={!props.taskComplete}
          >
            {props.btnMessage}
          </button>
        </div>
      </div>
    </div>
  );
};

export default GameFooter;
