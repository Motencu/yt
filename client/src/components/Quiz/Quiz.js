import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';

import QuizOption from './QuizOption';

class Quiz extends Component {
  constructor() {
    super();
    this.state = {
      card: {}
    };
  }

  componentDidMount() {
    Axios.get('../api/cards').then(res => {
      const rand = Math.floor(Math.random() * res.data.length);
      this.setState({ card: res.data[rand] });
      console.log(this.state.card);
    });
  }

  onClickAnswer = answer => {
    const updatedQuestion = this.state.question;
    updatedQuestion.isAnswered = true;
    updatedQuestion.selectedAnswer = answer.id;
    this.setState({ question: updatedQuestion });
    console.log(this.state.question);
  };

  render() {
    return (
      <div className="container-fluid box">
        <div className="row">
          <div className="col-1 text-center p-0">
            <Link className="btn-side" to="/">
              <i className="fas fa-home side-button" />
            </Link>
          </div>
          <div className="col-10 text-center p-0">
            <div id="quiz">
              <h1>{this.state.card.text}</h1>
              <div className="options">
                <div className="row" />
              </div>
            </div>
          </div>
          <div className="col-1 text-center p-0">
            <i className="fas fa-volume-up side-button" />
          </div>
        </div>
      </div>
    );
  }
}

export default Quiz;
