import React from 'react';

const QuizOption = props => {
  let buttonClasses = 'btn btn-answer';
  if (
    props.question.isAnswered &&
    props.question.correctAnswer === props.answer.id
  ) {
    buttonClasses += ' btn-correct';
  } else if (
    props.question.isAnswered &&
    props.question.selectedAnswer === props.answer.id &&
    props.question.selectedAnswer !== props.question.correctAnswer
  ) {
    buttonClasses += ' btn-incorrect';
  }
  return (
    <div className="col-6">
      <button
        className={buttonClasses}
        onClick={() => props.onClickAnswer(props.answer)}
        disabled={props.question.isAnswered}
      >
        {props.answer.text}
      </button>
    </div>
  );
};

export default QuizOption;
