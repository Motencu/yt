import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import { Provider } from 'react-redux';
import store from './store';

import PrivateRoute from './components/common/PrivateRoute';
import AdminRoute from './components/common/AdminRoute';

import Menu from './components/Menu';
import VideoMain from './components/Videos/Index';
import VideoKids from './components/Videos/VideoMain';
import Play from './components/Play/CountModule';
import Quiz from './components/Quiz/Quiz';
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import AdminDashboard from './components/Admin/AdminDashboard';
import AdminCards from './components/Admin/Cards/Cards';
import AdminCard from './components/Admin/Card/Card';

if (localStorage.jwtToken) {
  // Set token to Auth header
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  //set current user
  store.dispatch(setCurrentUser(decoded));

  //check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Route exact path="/" component={VideoMain} />

            <div className="container-fluid">
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
            </div>

            <Switch>
              <Route path="/play" component={VideoKids} />{' '}
            </Switch>
            <Switch>
              <PrivateRoute path="/quiz" component={Quiz} />{' '}
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin" component={AdminCards} />{' '}
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/cards" component={AdminCards} />{' '}
            </Switch>
            <Switch>
              <AdminRoute exact path="/admin/card/:id" component={AdminCard} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
