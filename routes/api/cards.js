const express = require('express');
const router = express.Router();
const passport = require('passport');

// load card model
const Card = require('../../models/Card');

// validation
const validateCardInput = require('../../validation/card');

// @route    GET api/cards/test
// @desc     Test route
// @access   Public
router.get('/test', (req, res) => {
  res.json({ msg: 'success' });
});

// @route    GET api/cards
// @desc     Get cards
// @access   Public
router.get('/', (req, res) => {
  Card.find()
    .sort({ date: -1 })
    .then(cards => res.json(cards))
    .catch(err => res.status(404).json({ nocardsfound: 'No cards found' }));
});

// @route    GET api/cards/:id
// @desc     Get one card
// @access   Public
router.get('/:id', (req, res) => {
  Card.findById(req.params.id)
    .then(card => res.json(card))
    .catch(err =>
      res.status(404).json({ nocardfound: 'No card found with that ID' })
    );
});

// @route    POST api/cards
// @desc     Create card
// @access   Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateCardInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }
    const newCard = new Card({
      audio: req.body.audio,
      text: req.body.text,
      user: req.user.id
    });

    newCard.save().then(card => res.json(card));
  }
);

// @route    DELETE api/cards/:id
// @desc     Delete a card
// @access   Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Card.findById(req.params.id)
      .then(card => {
        // Delete
        card.remove().then(() => res.json({ success: true }));
      })
      .catch(err =>
        res.status(404).json({ nocardfound: 'No card found with that ID' })
      );
  }
);

// @route    POST api/cards/options/:id
// @desc     Add an option to card
// @access   Private
router.post(
  '/options/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateCardInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }
    Card.findById(req.params.id)
      .then(card => {
        // add option to card
        const newOption = {
          text: req.body.text
        };
        card.options.push(newOption);

        card.save().then(card => res.json(card));
      })
      .catch(err =>
        res.status(404).json({ nocardfound: 'No card found with that ID' })
      );
  }
);

// @route    POST api/cards/options/:id/:option_id
// @desc     Delete an option to card
// @access   Private
router.delete(
  '/options/:id/:option_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Card.findById(req.params.id)
      .then(card => {
        // Check if option exists
        if (
          card.options.filter(
            option => option._id.toString() === req.params.option_id
          ).length === 0
        ) {
          return res
            .status(404)
            .json({ optionnotexists: 'option does not exist' });
        }

        // get remove index
        const removeIndex = card.options
          .map(item => item._id.toString())
          .indexOf(req.params.option_id);

        // splice option out of array
        card.options.splice(removeIndex, 1);

        card.save().then(card => res.json(card));
      })
      .catch(err =>
        res.status(404).json({ nocardfound: 'No card found with that ID' })
      );
  }
);

// @route    PUT api/cards/correct/:id
// @desc     Make option the correct answer
// @access   Private
router.put(
  '/correct/:id/:option_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Card.findById(req.params.id)
      .then(card => {
        // Check if option exists
        if (
          card.options.filter(
            option => option._id.toString() === req.params.option_id
          ).length === 0
        ) {
          return res
            .status(404)
            .json({ optionnotexists: 'option does not exist' });
        }

        // update correct answer
        card.correctId = req.params.option_id;

        card.save().then(card => res.json(card));
      })
      .catch(err =>
        res.status(404).json({ nocardfound: 'No card found with that ID' })
      );
  }
);

module.exports = router;
